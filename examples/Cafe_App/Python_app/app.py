from flask import Flask, render_template

app = Flask(__name__)

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/menu')
def menu():
    return render_template('menu.html')

@app.route('/order')
def order():
    return render_template('order.html')

@app.route('/confirmation', methods=['POST'])
def confirmation():
    # Process the order confirmation here
    return render_template('confirmation.html')


if __name__ == '__main__':
    app.run(debug=True)
